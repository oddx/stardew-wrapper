# stardew-wrapper
simple nix flake to run stardew valley on asahi nixos without steam or gog. should work for any aarch64 nixos installation

## setup
- follow the guides [here](https://www.reddit.com/r/AsahiLinux/comments/13jpsc7/stardew_valley_works_on_asahi/) and [here](https://web.archive.org/web/20200115233340/ur.gs/post/stardew-valley/) to get the compatibility version manifest from steam (I think the original guide works with GOG)
- (optional) export your save files and put them in ~/.config/StardewValley/Saves/
- put this flake in the directory containing StardewValley.exe
- run `nix shell`
- within the shell run `stardew-wrapper`

## caveats
as of now, I couldn't figure out how to get the sound part to work. PRs welcome :D
