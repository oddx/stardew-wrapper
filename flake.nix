{
  description = "Stardew Valley wrapper";

  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";

  outputs = {
    self,
    nixpkgs,
  }: let pkgs = nixpkgs.legacyPackages.aarch64-linux; in {

    # Package
    packages.aarch64-linux.default = pkgs.writeShellApplication {
      name = "stardew-wrapper";
      text = ''
        export LD_LIBRARY_PATH="${pkgs.SDL2}/lib:${pkgs.alsa-lib}/lib:${pkgs.openal}/lib"
        "${pkgs.mono}/bin/mono" StardewValley.exe
      '';
    };

    # Dev shell
    devShells.aarch64-linux.default = pkgs.mkShell {
      packages = with pkgs; [
        self.packages.aarch64-linux.default
      ];
    };
  };
}
